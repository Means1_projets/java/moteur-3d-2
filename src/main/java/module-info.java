module com.example.demo1 {
    requires static lombok;
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    exports com.example.demo1.demo;
    opens com.example.demo1.demo to javafx.fxml;
    exports com.example.demo1.api;
    opens com.example.demo1.api to javafx.fxml;
    exports com.example.demo1.api.math;
    opens com.example.demo1.api.math to javafx.fxml;
}