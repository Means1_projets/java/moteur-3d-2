package com.example.demo1.api;

import com.example.demo1.api.math.Mat4;
import com.example.demo1.api.math.Vector3;
import com.example.demo1.api.math.Vector4;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.Getter;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ApplicationManager extends Application {

    @Getter
    private static ApplicationManager instance;

    private static final float FOV = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;
    private Mat4 projection,projection2;
    protected GraphicsContext graphicsContext2D;
    private List<TriangleMesh> trianglesMesh = new ArrayList<>();
    private List<Light> lights = Arrays.asList(
            new Light(new Vector3(0, 0, 0),
                    new Vector3(0.45, 0.45, 0.3),
                    new Color(0.3, 0, 0, 1),
                    new Color(0.5, 0, 0, 1),
                    LightType.DIRECTION));
    private HelloController helloController;
    @Getter
    private Camera camera;
    private Mat4 viewMatrix;
    private Mat4 viewportMatrix;

    @Override
    public void start(Stage stage) throws IOException {
        instance = this;
        FXMLLoader fxmlLoader = new FXMLLoader(ApplicationManager.class.getClassLoader().getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 700, 500);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.show();
        camera = new Camera(stage);
        helloController = fxmlLoader.getController();
        graphicsContext2D = helloController.getCanvas().getGraphicsContext2D();

        float fAspectRatio = ((float) scene.getWidth()) / ((float) scene.getHeight());
        float y_scale = (float) ((1.0F / Math.tan(Math.toRadians(FOV / 2f))) * fAspectRatio);
        float x_scale = y_scale / fAspectRatio;
        float length = FAR_PLANE - NEAR_PLANE;
        float r = -1;
        float l = 1;
        float t = -1;
        float b = 1;
        float f = 1000;
        float n = 1;
        projection2 = new Mat4(
                2/(r-l), 0, 0, -(r+l)/(r-l),
                0, 2/(t-b), 0, -(t+b)/(t-b),
                0, 0, -2/(f-n), -(f+n)/(f-n),
                0, 0, 0, 1);
        projection = new Mat4(x_scale, 0, 0, 0,
                0, y_scale, 0, 0,
                0, 0, ((FAR_PLANE + NEAR_PLANE) / length), ((2 * NEAR_PLANE * FAR_PLANE) / length),
                0, 0, -1, 0);
        viewportMatrix = new Mat4(-scene.getWidth()/2, 0, 0, scene.getWidth()/2,
                0, scene.getHeight()/2, 0, scene.getHeight()/2,
                0, 0, 0.50, 0.50,
                0, 0, 0, 1);

        System.out.println(projection);

        System.out.println(viewportMatrix);
        startApp(stage, graphicsContext2D);
        new Thread(() -> {
            while (stage.isShowing()) {
                try {
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    graphicsContext2D = helloController.getCanvas().getGraphicsContext2D();
                    graphicsContext2D.clearRect(0, 0, 1000, 1000);

                    update();
                    camera.update();
                    viewMatrix = camera.createViewMatrix();
                    printAll();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void printAll() {
        List<ScreenTriangle> collect = trianglesMesh.stream().map(this::printOne).flatMap(Collection::stream).collect(Collectors.toList());

        if (camera.isDepthTest())
            collect = collect.stream().filter(p -> p.getBaricentre().getZ() < 0).sorted(Comparator.comparingDouble(p -> -p.getBaricentre().getZ())).collect(Collectors.toList());
        if (camera.isActiveCullingFace())
            collect = collect.stream().filter(p -> {
                Vector3 dot = p.getNormal();
                return dot.getZ() > 0;
            }).collect(Collectors.toList());

        int cap = 1000;
        collect = collect.stream().filter(p -> p.getV0().length() < cap && p.getV1().length() < cap && p.getV1().length() < cap).collect(Collectors.toList());

        printTriangles(collect);
    }

    public abstract void startApp(Stage stage, GraphicsContext graphicsContext2D);

    public abstract void update();


    protected void registerEntity(TriangleMesh triangleMesh){
        trianglesMesh.add(triangleMesh);
    }

    public List<ScreenTriangle> printOne(TriangleMesh triangleMesh){
        Mat4 mul = viewportMatrix.mul(camera.isSwitchProjection() ? projection : projection2).mul(viewMatrix);
        Mat4 mul2 = triangleMesh.createTransformation();
        AtomicInteger i = new AtomicInteger();
        return triangleMesh.getFaces().stream()
                .map(p -> computeTriangle(p, triangleMesh, mul, mul2, i)).collect(Collectors.toList());
    }

    private ScreenTriangle computeTriangle(int[] p, TriangleMesh triangleMesh, Mat4 mul, Mat4 mul2, AtomicInteger i) {
        Vector4 v0 = mul2.mul(triangleMesh.getVector3List().get(p[0]).cart2hom());
        Vector4 v1 = mul2.mul(triangleMesh.getVector3List().get(p[1]).cart2hom());
        Vector4 v2 = mul2.mul(triangleMesh.getVector3List().get(p[2]).cart2hom());
        Color color = triangleMesh.getColors().get(i.getAndIncrement());

        if (camera.isLightActive()) {
            Color finalColor = color;
            color = lights.stream().map(l -> l.processColor(finalColor, v0.hom2cart(), v1.hom2cart(), v2.hom2cart())).reduce(
                new Color(0, 0, 0, 0),
                (a, b) -> new Color(a.getRed() + b.getRed(), b.getGreen() + a.getGreen(), a.getBlue() + b.getBlue(), 1));
        }



        ScreenTriangle screenTriangle = new ScreenTriangle(mul.mul(v0).hom2cart(),
                mul.mul(v1).hom2cart(),
                mul.mul(v2).hom2cart(), color);


        Vector3 normal = Vector3.normal(v0.hom2cart(), v1.hom2cart(), v2.hom2cart());
        double dot = camera.getDir().dot(normal);

        if (Math.abs(dot) > 0.0001 && camera.isActiveSelection()) {
            double t = (v0.hom2cart().sub(camera.getPos()).dot(normal, false)) / dot;
            Vector3 hit = new Vector3(camera.getPos()).add(camera.getDir().mul(t));

            Vector3 r1 = Vector3.normal(hit, v0.hom2cart(), v1.hom2cart());
            Vector3 r2 = Vector3.normal(hit, v1.hom2cart(), v2.hom2cart());
            Vector3 r3 = Vector3.normal(hit, v2.hom2cart(), v0.hom2cart());

            if (normal.dot(r1) > 0 && normal.dot(r2) > 0 && normal.dot(r3) > 0) {
                screenTriangle.hit(mul.mul(hit.cart2hom()).hom2cart());
            }
        }
        return screenTriangle;
    }

    private void printTriangles(List<ScreenTriangle> triangles){
        for (ScreenTriangle screenTriangle  : triangles) {
            Vector3 ints1 = screenTriangle.getV0();
            Vector3 ints2 = screenTriangle.getV1();
            Vector3 ints3 = screenTriangle.getV2();
            if (camera.isColoredTriangle()) {
                if (!screenTriangle.isHit())
                graphicsContext2D.setFill(screenTriangle.getColor());
                else
                    graphicsContext2D.setFill(Color.BEIGE);
                graphicsContext2D.fillPolygon(
                        new double[]{ints1.getX(), ints2.getX(), ints3.getX()},
                        new double[]{ints1.getY(), ints2.getY(), ints3.getY()}, 3);
                if (screenTriangle.isHit()){
                    graphicsContext2D.setFill(Color.BLACK);
                    Vector3 baricentre = screenTriangle.getBaricentre();
                    graphicsContext2D.fillOval(baricentre.getX(), baricentre.getY(), 10, 10);
                }
            }
            else {
                graphicsContext2D.setFill(Color.BLACK);
                graphicsContext2D.strokePolygon(
                        new double[]{ints1.getX(), ints2.getX(), ints3.getX()},
                        new double[]{ints1.getY(), ints2.getY(), ints3.getY()}, 3);
            }
        }
    }

}