package com.example.demo1.api;

import com.example.demo1.api.math.Mat4;
import com.example.demo1.api.math.Vector3;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

public class Camera {

    @Getter
    @Setter
    private Vector3 pos = new Vector3(0, 0, 0);
    private float pitch = 0;
    private float yaw = 0;
    private double speed = 0.2f;
    private Set<KeyCode> keypressed = new HashSet<>();
    private Set<KeyCode> whitelist = new HashSet<>(Set.of(KeyCode.Z, KeyCode.D, KeyCode.Q, KeyCode.S, KeyCode.SPACE, KeyCode.SHIFT));
    @Getter
    private boolean switchProjection = true, activeCullingFace = true, coloredTriangle = true, depthTest = true, activeSelection = false, lightActive = false;
    private double posX, posY;
    private boolean hasRelease;
    /**
     * Constructeur de la class Caméra
     */
    public Camera(Stage stage) {
        posX = stage.getScene().getWidth() / 2;
        posY = stage.getScene().getHeight() / 2;
        //Mouse.setCursorPosition(Display.getWidth() / 2, Display.getHeight() / 2);
        stage.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if (whitelist.contains(key.getCode())){
                keypressed.add(key.getCode());
            }
            if (key.getCode() == KeyCode.P){
                switchProjection ^= true;
            }
            if (key.getCode() == KeyCode.I){
                activeCullingFace ^= true;
            }
            if (key.getCode() == KeyCode.U){
                coloredTriangle ^= true;
            }
            if (key.getCode() == KeyCode.Y){
                depthTest ^= true;
            }
            if (key.getCode() == KeyCode.T){
                activeSelection  ^= true;
            }
            if (key.getCode() == KeyCode.R){
                lightActive  ^= true;
            }
        });

        stage.addEventHandler(KeyEvent.KEY_RELEASED, (key) -> {
            if (whitelist.contains(key.getCode())){
                keypressed.remove(key.getCode());
            }
        });
        stage.getScene().setOnMouseReleased(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)){
                hasRelease = true;
            }
        });
        stage.getScene().setOnMouseDragged(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (hasRelease) {
                    posX = event.getSceneX();
                    posY = event.getSceneY();
                    hasRelease = false;
                    return;
                }
                float speed = 0.2f;
                yaw = ((yaw + ((float) (posX - event.getSceneX())) * speed) + 360) % 360;
                float pitch2 = ((float) (posY - event.getSceneY())) * speed;
                if (pitch - pitch2 > 90) {
                    pitch = 90;
                } else if (pitch - pitch2 < -90) {
                    pitch = -90;
                } else {
                    pitch -= pitch2;
                }
                posX = event.getSceneX();
                posY = event.getSceneY();
            }
        });
    }


    /**
     * Retourne la Matrice qui est la ViewMatrix
     * @return Matrix4f
     */
    public Mat4 createViewMatrix() {
        Mat4 matrix4f = new Mat4();
        matrix4f.identity();
        matrix4f.rotate((float) Math.toRadians(pitch), new Vector3(1, 0, 0));

        matrix4f.rotate((float) Math.toRadians(yaw), new Vector3(0, 1, 0));

        matrix4f.translate(new Vector3(-pos.getX(), -pos.getY(), -pos.getZ()));
        return matrix4f;
    }

    /**
     * Retourne le vecteur qui est la direction dans laquelle le joueur regarde
     * @return
     */
    public Vector3 getDir() {
        float yaw = this.yaw + 90;
        float pitch = this.pitch;
        return new Vector3((float) (Math.cos(Math.toRadians(yaw)) * Math.cos(Math.toRadians(pitch))),
                (float) Math.sin(Math.toRadians(pitch)), (float) (Math.cos(Math.toRadians(pitch)) * Math.sin(Math.toRadians(yaw))));
    }

    /**
     * Update de la caméra quand une action est faite (keys)
     */
    public void update() {
        if(keypressed.contains(KeyCode.Z)) {
            pos.add(new Vector3((float) (speed * Math.cos(Math.toRadians(yaw + 90))), 0, (float) (speed * Math.sin(Math.toRadians(yaw + 90)))));
        }
        if(keypressed.contains(KeyCode.S)) {
            pos.add(new Vector3((float) (-speed * Math.cos(Math.toRadians(yaw + 90))), 0, (float) (-speed * Math.sin(Math.toRadians(yaw + 90)))));
        }
        if(keypressed.contains(KeyCode.Q)) {
            pos.add(new Vector3((float) (-speed * Math.cos(Math.toRadians(yaw))), 0, (float) (-speed * Math.sin(Math.toRadians(yaw)))));
        }
        if(keypressed.contains(KeyCode.D)) {
            pos.add(new Vector3((float) (speed * Math.cos(Math.toRadians(yaw))), 0, (float) (speed * Math.sin(Math.toRadians(yaw)))));
        }
        if(keypressed.contains(KeyCode.SPACE)) {
            pos.add(new Vector3(0, speed, 0));
        }
        if(keypressed.contains(KeyCode.SHIFT)) {
            pos.add(new Vector3(0, -speed, 0));
        }
    }
}
