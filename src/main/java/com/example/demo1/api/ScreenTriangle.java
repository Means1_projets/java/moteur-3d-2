package com.example.demo1.api;

import com.example.demo1.api.math.Vector3;
import javafx.scene.paint.Color;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ScreenTriangle {

    private Vector3 v0, v1, v2;
    private Color color;
    private boolean hit;

    public ScreenTriangle(Vector3 v0, Vector3 v1, Vector3 v2, Color color) {
        this.v0 = v0;
        this.v1 = v1;
        this.v2 = v2;
        this.color = color;
    }

    public Vector3 getBaricentre() {
        return new Vector3((v0.getX() + v1.getX() + v2.getX()) / 3d,
                (v0.getY() + v1.getY() + v2.getY()) / 3d,
                (v0.getZ() + v1.getZ() + v2.getZ()) / 3d);
    }

    public Vector3 getNormal() {
        return Vector3.normal(v0, v1, v2);
    }

    public void hit(Vector3 hit) {
        System.out.println("hit");
        this.hit = true;
    }


}
