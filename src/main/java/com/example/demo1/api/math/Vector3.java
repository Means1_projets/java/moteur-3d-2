package com.example.demo1.api.math;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Vector3 {

    private double x, y, z;

    public Vector3(Vector3 vector3){
        this.x = vector3.x;
        this.y = vector3.y;
        this.z = vector3.z;
    }

    public static Vector3 normal(Vector3 v0, Vector3 v1, Vector3 v2) {
        return new Vector3(v0.getX() - v1.getX(),
                v0.getY() - v1.getY(),
                v0.getZ() - v1.getZ())
                .cross(new Vector3(v0.getX() - v2.getX(),
                        v0.getY() - v2.getY(),
                        v0.getZ() - v2.getZ())).normalize();
    }


    public double length(){
        return Math.sqrt(x*x+y*y+z*z);
    }

    public Vector3 normalize(){
        double length = length();
        if (length < 0.001){
            x = 0;
            y = 0;
            z = 0;
            return this;
        }
        x /= length;
        y /= length;
        z /= length;
        return this;
    }

    public static Vector3 normalize(Vector3 vector3){
        return new Vector3(vector3).normalize();
    }

    public Vector3 neg(){
        mul(-1);
        return this;
    }

    public static Vector3 neg(Vector3 vector3){
        return new Vector3(vector3).neg();
    }

    public Vector3 mul(double k){
        x *= k;
        y *= k;
        z *= k;
        return this;
    }

    public static Vector3 neg(Vector3 vector3, double k){
        return new Vector3(vector3).mul(k);
    }

    public Vector3 add(Vector3 w){
        x += w.x;
        y += w.y;
        z += w.z;
        return this;
    }

    public static Vector3 add(Vector3 vector3, Vector3 w){
        return new Vector3(vector3).add(w);
    }

    public Vector3 sub(Vector3 w){
        x -= w.x;
        y -= w.y;
        z -= w.z;
        return this;
    }

    public static Vector3 sub(Vector3 vector3, Vector3 w){
        return new Vector3(vector3).sub(w);
    }
    public double dot(Vector3 w) {
        return dot(w, true);
    }
    public double dot(Vector3 w, boolean normalise){
        double d = this.x * w.x + this.y * w.y + this.z * w.z;
        double length = length();
        double length1 = w.length();
        if (!normalise || length < 0.001 || length1 < 0.001)
            return d;
        return d/(length*length1);
    }

    public Vector3 cross(Vector3 w){
        return new Vector3(
                this.y * w.z - this.z * w.y,
                this.z * w.x - this.x * w.z,
                this.x * w.y - this.y * w.x
        );
    }

    public Vector4 cart2hom() {
        return new Vector4(this.x, this.y, this.z, 1);
    }

    public void set(Vector3 fromQuaternion) {
        this.x = fromQuaternion.getX();
        this.y = fromQuaternion.getY();
        this.z = fromQuaternion.getZ();
    }

    public static Vector3 getBaricentre(Vector3 v0, Vector3 v1, Vector3 v2) {
        return new Vector3((v0.getX() + v1.getX() + v2.getX()) / 3d,
                (v0.getY() + v1.getY() + v2.getY()) / 3d,
                (v0.getZ() + v1.getZ() + v2.getZ()) / 3d);
    }
    public static double distance(Vector3 v0, Vector3 v1) {
        return Math.sqrt(Math.pow(v0.getX()-v1.getX(), 2) + Math.pow(v0.getY()-v1.getY(), 2) + Math.pow(v0.getZ()-v1.getZ(), 2));
    }
}
