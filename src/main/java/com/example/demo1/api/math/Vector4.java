package com.example.demo1.api.math;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Vector4 {

    private double x, y, z, t;

    public Vector4(Vector4 vector4){
        this.x = vector4.x;
        this.y = vector4.y;
        this.z = vector4.z;
        this.t = vector4.t;
    }


    public double length(){
        return Math.sqrt(x*x+y*y+z*z+t*t);
    }

    public Vector4 normalize(){
        double length = length();
        x /= length;
        y /= length;
        z /= length;
        t /= length;
        return this;
    }

    public static Vector4 normalize(Vector4 vector4){
        return new Vector4(vector4).normalize();
    }

    public Vector4 neg(){
        mul(-1);
        return this;
    }

    public static Vector4 neg(Vector4 vector4){
        return new Vector4(vector4).neg();
    }

    public Vector4 mul(double k){
        x *= k;
        y *= k;
        z *= k;
        t *= k;
        return this;
    }

    public static Vector4 neg(Vector4 vector4, double k){
        return new Vector4(vector4).mul(k);
    }

    public Vector4 add(Vector4 w){
        x += w.x;
        y += w.y;
        z += w.z;
        t += w.t;
        return this;
    }

    public static Vector4 add(Vector4 vector4, Vector4 w){
        return new Vector4(vector4).add(w);
    }

    public Vector4 sub(Vector4 w){
        x -= w.x;
        y -= w.y;
        z -= w.z;
        t -= w.t;
        return this;
    }

    public static Vector4 sub(Vector4 vector4, Vector4 w){
        return new Vector4(vector4).sub(w);
    }

    public double dot(Vector4 w){
        return this.x * w.x + this.y * w.y + this.z * w.z + this.t * w.t;
    }

    public Vector3 hom2cart() {
        return new Vector3(this.x/this.t, this.y/this.t, this.z/this.t);
    }
}
