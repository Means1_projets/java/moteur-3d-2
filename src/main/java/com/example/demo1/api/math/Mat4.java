package com.example.demo1.api.math;

import lombok.Data;

@Data
public class Mat4 {

    private double[][] data;

    public Mat4(Mat4 mat4){
        this(mat4.data[0][0], mat4.data[0][1], mat4.data[0][2], mat4.data[0][3],
                mat4.data[1][0], mat4.data[1][1], mat4.data[1][2], mat4.data[1][3],
                mat4.data[2][0], mat4.data[2][1], mat4.data[2][2], mat4.data[2][3],
                mat4.data[3][0], mat4.data[3][1], mat4.data[3][2], mat4.data[3][3]
        );
    }

    public Mat4(double r11, double r12, double r13, double r14,
                double r21, double r22, double r23, double r24,
                double r31, double r32, double r33, double r34,
                double r41, double r42, double r43, double r44){
        this.data = new double[][]{
                { r11, r12, r13, r14 },
                { r21, r22, r23, r24 },
                { r31, r32, r33, r34 },
                { r41, r42, r43, r44 }
        };
    }

    public Mat4() {
        this.data = new double[][]{
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 },
                { 0, 0, 0, 0 }
        };
    }

    public Mat4 identity(){
        for (int i = 0; i < 4; i++) {
            this.data[i][i] = 1;
        }
        return this;
    }

    public Mat4 mul(double k){
        for (int i = 0; i < data.length; i++) {
            for (int i1 = 0; i1 < data[i].length; i1++) {
                data[i][i1] *= k;
            }
        }
        return this;
    }


    public Mat4 mul(Mat4 n){
        Mat4 mat4 = new Mat4(n);
        for (int i = 0; i < data.length; i++) {
            for (int i1 = 0; i1 < data[i].length; i1++) {
                mat4.data[i][i1] = 0;
                for (int i2 = 0; i2 < data[i].length; i2++) {
                    mat4.data[i][i1] += this.data[i][i2] * n.data[i2][i1];
                }
            }
        }
        return mat4;
    }

    public Mat4 scale(Vector3 vec){
        this.data[0][0] = this.data[0][0] * vec.getX();
        this.data[1][0] = this.data[1][0] * vec.getX();
        this.data[2][0] = this.data[2][0] * vec.getX();
        this.data[3][0] = this.data[3][0] * vec.getX();
        this.data[0][1] = this.data[0][1] * vec.getY();
        this.data[1][1] = this.data[1][1] * vec.getY();
        this.data[2][1] = this.data[2][1] * vec.getY();
        this.data[3][1] = this.data[3][1] * vec.getY();
        this.data[0][2] = this.data[0][2] * vec.getZ();
        this.data[1][2] = this.data[1][2] * vec.getZ();
        this.data[2][2] = this.data[2][2] * vec.getZ();
        this.data[3][2] = this.data[3][2] * vec.getZ();
        return this;
    }

    public Mat4 rotate(float angle, Vector3 axis){
        double c = (float)Math.cos(angle);
        double s = (float)Math.sin(angle);
        double oneminusc = 1.0F - c;
        double xy = axis.getX() * axis.getY();
        double yz = axis.getY() * axis.getZ();
        double xz = axis.getX() * axis.getZ();
        double xs = axis.getX() * s;
        double ys = axis.getY() * s;
        double zs = axis.getZ() * s;
        double f00 = axis.getX() * axis.getX() * oneminusc + c;
        double f01 = xy * oneminusc + zs;
        double f02 = xz * oneminusc - ys;
        double f10 = xy * oneminusc - zs;
        double f11 = axis.getY() * axis.getY() * oneminusc + c;
        double f12 = yz * oneminusc + xs;
        double f20 = xz * oneminusc + ys;
        double f21 = yz * oneminusc - xs;
        double f22 = axis.getZ() * axis.getZ() * oneminusc + c;
        double t00 = this.data[0][0] * f00 + this.data[0][1] * f01 + this.data[0][2] * f02;
        double t01 = this.data[1][0] * f00 + this.data[1][1] * f01 + this.data[1][2] * f02;
        double t02 = this.data[2][0] * f00 + this.data[2][1] * f01 + this.data[2][2] * f02;
        double t03 = this.data[3][0] * f00 + this.data[3][1] * f01 + this.data[3][2] * f02;
        double t10 = this.data[0][0] * f10 + this.data[0][1] * f11 + this.data[0][2] * f12;
        double t11 = this.data[1][0] * f10 + this.data[1][1] * f11 + this.data[1][2] * f12;
        double t12 = this.data[2][0] * f10 + this.data[2][1] * f11 + this.data[2][2] * f12;
        double t13 = this.data[3][0] * f10 + this.data[3][1] * f11 + this.data[3][2] * f12;
        this.data[0][2] = this.data[0][0] * f20 + this.data[0][1] * f21 + this.data[0][2] * f22;
        this.data[1][2] = this.data[1][0] * f20 + this.data[1][1] * f21 + this.data[1][2] * f22;
        this.data[2][2] = this.data[2][0] * f20 + this.data[2][1] * f21 + this.data[2][2] * f22;
        this.data[3][2] = this.data[3][0] * f20 + this.data[3][1] * f21 + this.data[3][2] * f22;
        this.data[0][0] = t00;
        this.data[1][0] = t01;
        this.data[2][0] = t02;
        this.data[3][0] = t03;
        this.data[0][1] = t10;
        this.data[1][1] = t11;
        this.data[2][1] = t12;
        this.data[3][1] = t13;
        return this;
    }

    public Mat4 translate(Vector3 vec) {
        this.data[0][3] += this.data[0][0] * vec.getX() + this.data[0][1] * vec.getY() + this.data[0][2] * vec.getZ();
        this.data[1][3] += this.data[1][0] * vec.getX() + this.data[1][1] * vec.getY() + this.data[1][2] * vec.getZ();
        this.data[2][3] += this.data[2][0] * vec.getX() + this.data[2][1] * vec.getY() + this.data[2][2] * vec.getZ();
        this.data[3][3] += this.data[3][0] * vec.getX() + this.data[3][1] * vec.getY() + this.data[3][2] * vec.getZ();
        return this;
    }


    public Vector4 mul(Vector4 v){
        return new Vector4(
                v.getX()*data[0][0]+v.getY()*data[0][1]+v.getZ()*data[0][2]+v.getT()*data[0][3],
                v.getX()*data[1][0]+v.getY()*data[1][1]+v.getZ()*data[1][2]+v.getT()*data[1][3],
                v.getX()*data[2][0]+v.getY()*data[2][1]+v.getZ()*data[2][2]+v.getT()*data[2][3],
                v.getX()*data[3][0]+v.getY()*data[3][1]+v.getZ()*data[3][2]+v.getT()*data[3][3]);
    }


    /*public Vector4 mul(Vector4 v){
        return new Vector4(
                v.getX()*data[0][0]+v.getY()*data[1][0]+v.getZ()*data[2][0]+v.getT()*data[3][0],
                v.getX()*data[0][1]+v.getY()*data[1][1]+v.getZ()*data[2][1]+v.getT()*data[3][1],
                v.getX()*data[0][2]+v.getY()*data[1][2]+v.getZ()*data[2][2]+v.getT()*data[3][2],
                v.getX()*data[0][3]+v.getY()*data[1][3]+v.getZ()*data[2][3]+v.getT()*data[3][3]);
    }*/
}
