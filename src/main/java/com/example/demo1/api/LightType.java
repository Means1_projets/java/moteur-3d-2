package com.example.demo1.api;

import lombok.Getter;

public enum LightType {
    ALLDIR(0),
    DIRECTION(1);
//    FLASHLIGHT(2);


    @Getter
    private final int id;

    LightType(int i) {

        id = i;
    }
}