package com.example.demo1.api;

import com.example.demo1.api.math.Vector3;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Light {

    private final Vector3 pos, dir;
    private final Color color_ambiant;
    private final Color color_diffuse;
    private final LightType lightType;
    private float constant = 1.0F;
    private float linear = 0.045F;
    private float quadratic = 0.0016F;
    private float cutOff = (float) Math.cos(Math.toRadians(17.5));
    private float outerCutOff = (float) Math.cos(Math.toRadians(12.5));

    public Color processColor(Color color, Vector3 v0, Vector3 v1, Vector3 v2) {
        if (lightType.equals(LightType.ALLDIR)){
            Vector3 baricentre = Vector3.getBaricentre(v0, v1, v2);

            double distance = Vector3.distance(baricentre, pos);
            double attenuation = 1.0 / (constant + linear * distance +
                    quadratic * (distance * distance));

            Color ambient = new Color(color.getRed()*this.color_ambiant.getRed(), color.getGreen()*this.color_ambiant.getGreen(), color.getBlue()*this.color_ambiant.getBlue(), 1);

            // diffuse
            Vector3 norm = Vector3.normal(v0, v1, v2);
            Vector3 lightDir = new Vector3(pos.getX() - baricentre.getX(), pos.getY() - baricentre.getY(), pos.getZ() - baricentre.getZ()).normalize();
            double diff = Math.max(norm.dot(lightDir), 0.0);
            Color diffuse = new Color(color.getRed()*this.color_diffuse.getRed()*diff, color.getGreen()*this.color_diffuse.getGreen()*diff, color.getBlue()*this.color_diffuse.getBlue()*diff, 1);
            return new Color((ambient.getRed()+diffuse.getRed())*attenuation,
                    (ambient.getGreen()+diffuse.getGreen())*attenuation,
                    (ambient.getBlue()+diffuse.getBlue())*attenuation, 1);
        } else if (lightType.equals(LightType.DIRECTION)){
            Color ambient = new Color(color.getRed()*this.color_ambiant.getRed(), color.getGreen()*this.color_ambiant.getGreen(), color.getBlue()*this.color_ambiant.getBlue(), 1);

            // diffuse
            Vector3 norm = Vector3.normal(v0, v1, v2);
            double diff = Math.max(norm.dot(dir), 0.0);
            Color diffuse = new Color(color.getRed()*this.color_diffuse.getRed()*diff, color.getGreen()*this.color_diffuse.getGreen()*diff, color.getBlue()*this.color_diffuse.getBlue()*diff, 1);
            return new Color(Math.min(1, ambient.getRed()+diffuse.getRed()),
                    Math.min(1, ambient.getGreen()+diffuse.getGreen()),
                    Math.min(1, ambient.getBlue()+diffuse.getBlue()), 1);
        }
        return null;
    }
}