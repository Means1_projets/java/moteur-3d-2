package com.example.demo1.api;

import com.example.demo1.api.math.Mat4;
import com.example.demo1.api.math.Vector3;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Data
public class TriangleMesh {

    private List<Vector3> vector3List;
    private List<int[]> faces;
    private Vector3 pos;
    private Vector3 rot;
    private float scale;
    private List<Color> colors = new ArrayList<>();
    private Optional<Mat4> slerp;

    /**
     * Création de la matrice de transformation
     * @return
     */
    public Mat4 createTransformation() {
        return slerp.orElseGet(() -> createTransformation(pos, rot, scale));
    }

    private static Mat4 createTransformation(Vector3 pos, Vector3 rot, float scale) {
        Mat4 matrix4f = new Mat4();
        matrix4f.identity();
        matrix4f.translate(pos);
        matrix4f.rotate((float) Math.toRadians(rot.getX()), new Vector3(1, 0, 0));
        matrix4f.rotate((float) Math.toRadians(rot.getY()), new Vector3(0, 1, 0));
        matrix4f.rotate((float) Math.toRadians(rot.getZ()), new Vector3(0, 0, 1));
        matrix4f.scale(new Vector3(scale, scale, scale));
        return matrix4f;
    }

    public void rotate(Vector3 vector3) {
        double v = (rot.getX() + vector3.getX() + 360) % 360;
        double v1 = Math.min(90, Math.max(rot.getY() + vector3.getY(), -90));
        double v2 = (rot.getZ() + vector3.getZ() + 360) % 360;
        rot = new Vector3(v > 180 ? v - 360 : v, v1, v2 > 180 ? v2 - 360 : v2);
    }

    public static double[] rightAngles(double[] rot){

        double v = (rot[0] + 360) % 360;
        double v1 = (rot[1] + 360) % 360;
        double v2 = (rot[2] + 360) % 360;

        if (v1 > 90 && v1 < 270){
            v = (v + 180) % 360;
            v2 = (v2 + 180) % 360;
            v1 = 180 - v1;
        } else if (v1 >= 270){
            v1 = v1 - 360;
        }
        return new double[]{v > 180 ? v - 360 : v, v1, v2 > 180 ? v2 - 360 : v2};
    }

    public static Mat4 fromEuler(Vector3 rot){
        return createTransformation(new Vector3(0, 0, 0), rot, 1);
    }

    public static Mat4 createTransformationMatrix(Vector3 position, double scale, double[] rotation) {
        Mat4 dest = new Mat4();
        double q00 = 2.0f * rotation[1] * rotation[1];
        double q11 = 2.0f * rotation[2] * rotation[2];
        double q22 = 2.0f * rotation[3] * rotation[3];
        double q01 = 2.0f * rotation[1] * rotation[2];
        double q02 = 2.0f * rotation[1] * rotation[3];
        double q03 = 2.0f * rotation[1] * rotation[0];
        double q12 = 2.0f * rotation[2] * rotation[3];
        double q13 = 2.0f * rotation[2] * rotation[0];
        double q23 = 2.0f * rotation[3] * rotation[0];

        dest.getData()[0][0] = (1.0f - q11 - q22) * scale;
        dest.getData()[1][0] = (q01 + q23) * scale;
        dest.getData()[2][0] = (q02 - q13) * scale;
        dest.getData()[3][0] = 0.0f;
        dest.getData()[0][1] = (q01 - q23) * scale;
        dest.getData()[1][1] = (1.0f - q22 - q00) * scale;
        dest.getData()[2][1] = (q12 + q03) * scale;
        dest.getData()[3][1] = 0.0f;
        dest.getData()[0][2] = (q02 + q13) * scale;
        dest.getData()[1][2] = (q12 - q03) * scale;
        dest.getData()[2][2] = (1.0f - q11 - q00) * scale;
        dest.getData()[3][2] = 0.0f;
        dest.getData()[0][3] = position.getX();
        dest.getData()[1][3] = position.getY();
        dest.getData()[2][3] = position.getZ();
        dest.getData()[3][3] = 1.0f;
        return dest;
    }
    
    public static double[] toQuaternion(Vector3 rot) {
        double[] rot2 = rightAngles(new double[]{rot.getX(), rot.getY(), rot.getZ()});

        double x = Math.toRadians(rot2[0]);
        double y = Math.toRadians(rot2[1]);
        double z = Math.toRadians(rot2[2]);
        double c1 = Math.cos(x/2d);
        double s1 = Math.sin(x/2d);
        double c2 = Math.cos(y/2d);
        double s2 = Math.sin(y/2d);
        double c3 = Math.cos(z/2d);
        double s3 = Math.sin(z/2d);

        return new double[]{
                c1*c2*c3+s1*s2*s3,
                s1*c2*c3-c1*s2*s3,
                c1*s2*c3+s1*c2*s3,
                c1*c2*s3-s1*s2*c3
        };
    }
    public static double[] setFromMat(Mat4 mat4) {
        double[] q = new double[4];
        double tr = mat4.getData()[0][0] + mat4.getData()[2][2] + mat4.getData()[2][2];
        float s;
        if (tr >= 0.0D) {
            s = (float)Math.sqrt(tr + 1.0D);
            q[0] = s * 0.5F;
            s = 0.5F / s;
            q[1] = -(mat4.getData()[1][2] - mat4.getData()[2][1]) * s;
            q[2] = -(mat4.getData()[2][0] - mat4.getData()[0][2]) * s;
            q[3] = -(mat4.getData()[0][1] - mat4.getData()[1][0]) * s;
        } else {
            double max = Math.max(Math.max(mat4.getData()[0][0], mat4.getData()[1][1]), mat4.getData()[2][2]);
            if (max == mat4.getData()[0][0]) {
                s = (float)Math.sqrt((mat4.getData()[0][0] - (mat4.getData()[1][1] + mat4.getData()[2][2])) + 1.0D);
                q[1] = -s * 0.5F;
                s = 0.5F / s;
                q[2] = -(mat4.getData()[1][0] + mat4.getData()[0][1]) * s;
                q[3] = -(mat4.getData()[0][2] + mat4.getData()[2][0]) * s;
                q[0] = (mat4.getData()[1][2] - mat4.getData()[2][1]) * s;
            } else if (max == mat4.getData()[1][1]) {
                s = (float)Math.sqrt((mat4.getData()[1][1] - (mat4.getData()[2][2] + mat4.getData()[0][0])) + 1.0D);
                q[2] = -s * 0.5F;
                s = 0.5F / s;
                q[3] = -(mat4.getData()[2][1] + mat4.getData()[1][2]) * s;
                q[1] = -(mat4.getData()[1][0] + mat4.getData()[0][1]) * s;
                q[0] = (mat4.getData()[2][0] - mat4.getData()[0][2]) * s;
            } else {
                s = (float)Math.sqrt((mat4.getData()[2][2] - (mat4.getData()[0][0] + mat4.getData()[1][1])) + 1.0D);
                q[3] = -s * 0.5F;
                s = 0.5F / s;
                q[1] = -(mat4.getData()[0][2] + mat4.getData()[2][0]) * s;
                q[2] = -(mat4.getData()[2][1] + mat4.getData()[1][2]) * s;
                q[0] = (mat4.getData()[0][1] - mat4.getData()[1][0]) * s;
            }
        }

        return q;
    }

    public static Vector3 fromQuaternion(double[] q) {
        float sp = (float) (-2.0f * ( q[2]*q[3] - q[0]*q[1] ));
        System.out.println(sp);
        double v = Math.toDegrees(Math.atan2(2 * (q[1] * q[2] + q[3] * q[0]), 1 - 2 * (q[2] * q[2] + q[3] * q[3])));
        double v1 = Math.toDegrees(Math.asin(2 * (q[1] * q[3] - q[0] * q[2])));
        double v2 = Math.toDegrees(Math.atan2(2 * (q[1] * q[0] + q[2] * q[3]), 1 - 2 * (q[3] * q[3] + q[0] * q[0])));
        return new Vector3(Math.toDegrees(v), Math.toDegrees(v1), Math.toDegrees(v2));
        /*
        System.out.println("old : "+new Vector3(v, v1, v2));
        if (Math.abs(sp) > 0.9999f) {

            double p = 1.570796f * sp;
            double h = Math.atan2(-q[1] * q[3] + q[0] * q[2], 0.5f - q[2] * q[2] - q[3] * q[3]);
            double b = 0.0f;
            return new Vector3(Math.toDegrees(h), Math.toDegrees(p), Math.toDegrees(b));
        } else {
            double p = Math.asin(sp);
            double h = Math.atan2(q[1] * q[3] + q[0] * q[2], 0.5f - q[1] * q[1] - q[2] * q[2]);
            double b = Math.atan2(q[1] * q[2] + q[0] * q[3], 0.5f - q[1] * q[1] - q[3] * q[3]);
            return new Vector3(h, p, b);
        }
*/
    }

}
