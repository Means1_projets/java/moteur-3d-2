package com.example.demo1.demo;

import com.example.demo1.api.math.Vector3;

public class Lerp {
    private double time, timeMax;
    private Vector3 dCo, finalCo;

    public Lerp(double timeMax, Vector3 actualCo, Vector3 finalCo){
        time = 0;
        this.timeMax = timeMax;
        this.finalCo = finalCo;
        this.dCo = new Vector3((finalCo.getX()-actualCo.getX()) / timeMax, (finalCo.getY()-actualCo.getY()) / timeMax, (finalCo.getZ()-actualCo.getZ()) / timeMax);
    }

    public void apply(Vector3 vector3){
        vector3.setX(vector3.getX() + dCo.getX());
        vector3.setY(vector3.getY() + dCo.getY());
        vector3.setZ(vector3.getZ() + dCo.getZ());
        if (++time >= timeMax){
            vector3.set(finalCo);
        }
    }

    public boolean finish(){
        return time >= timeMax;
    }
}
