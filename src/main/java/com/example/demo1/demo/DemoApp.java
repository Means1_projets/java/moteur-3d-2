package com.example.demo1.demo;

import com.example.demo1.api.ApplicationManager;
import com.example.demo1.api.TriangleMesh;
import com.example.demo1.api.math.Mat4;
import com.example.demo1.api.math.Vector3;
import com.example.demo1.api.math.Vector4;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class DemoApp extends ApplicationManager {

    private TriangleMesh triangleMesh;
    private TriangleMesh triangleMesh2;
    private int toggleRot = 0;
    private Lerp lerp;
    private Slerp slerp;

    @Override
    public void startApp(Stage stage, GraphicsContext graphicsContext2D) {
        this.graphicsContext2D = graphicsContext2D;
        triangleMesh = createCube(new Vector3(0, 0, 10), new Vector3(0, 0, 0), 1);
        triangleMesh2 = createCube(new Vector3(0, 0, 20), new Vector3(17, 42, 42), 3f);
        //registerEntity(triangleMesh2);
        registerEntity(triangleMesh);
        //registerEntity(axes(new Vector3(0, 0, 0), Color.RED));
        //registerEntity(axes(new Vector3(90, 0, 0), Color.BLUE));
        //registerEntity(axes(new Vector3(0, 90, 0), Color.GREEN));
        //registerEntity(createCube(new Vector3(0, 0, 20), new Vector3(0, 33, 30)));

        stage.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.getCode() == KeyCode.NUMPAD4) {
                triangleMesh.rotate(new Vector3(3, 0, 0));
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD6) {
                triangleMesh.rotate(new Vector3(-3, 0, 0));
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD8) {
                triangleMesh.rotate(new Vector3(0, 3, 0));
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD2) {
                triangleMesh.rotate(new Vector3(0, -3, 0));
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD7) {
                triangleMesh.rotate(new Vector3(0, 0, 3));
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD9) {
                triangleMesh.rotate(new Vector3(0, 0, -3));
            }
            if (keyEvent.getCode() == KeyCode.L) {
                if (lerp == null || lerp.finish()){
                    lerp = new Lerp(50, triangleMesh.getRot(), new Vector3(0, 0, 0));
                }
            }
            if (keyEvent.getCode() == KeyCode.M) {
                if (slerp == null || slerp.finish()){
                    slerp = new Slerp(50, triangleMesh.getRot(), new Vector3(0, 0, 0));
                }
            }
            if (keyEvent.getCode() == KeyCode.O){
                toggleRot = (toggleRot + 1) % 4;
            }
        });
    }

    private TriangleMesh axes(Vector3 vector3, Color color) {
        return new TriangleMesh(Arrays.asList(
                new Vector3(0, 0, 0),
                new Vector3(0, 0.25, 0),
                new Vector3(0, 0.25, 1.0)
                ), Arrays.asList(
                new int[]{0, 1, 2},
                new int[]{0, 2, 1}
        ), new Vector3(0, 0, 0), vector3, 1,
                Arrays.asList(color, color), Optional.empty());
    }

    private TriangleMesh createCube(Vector3 pos, Vector3 rot, float scale) {
        return new TriangleMesh(Arrays.asList(
                new Vector3(-1.0, -1.0, 1.0),
                new Vector3(1.0, -1.0, 1.0),
                new Vector3(-1.0, 1.0, 1.0),
                new Vector3(1.0, 1.0, 1.0),
                new Vector3(-1.0, 1.0, -1.0),
                new Vector3(1.0, 1.0, -1.0),
                new Vector3(-1.0, -1.0, -1.0),
                new Vector3(1.0, -1.0, -1.0)), Arrays.asList(
                new int[]{0, 1, 2}, new int[]{2, 1, 3}, new int[]{2, 3, 4},
                new int[]{4, 3, 5}, new int[]{4, 5, 6}, new int[]{6, 5, 7},
                new int[]{6, 7, 0}, new int[]{0, 7, 1}, new int[]{1, 7, 3},
                new int[]{3, 7, 5}, new int[]{6, 0, 4}, new int[]{4, 0, 2}
        ), pos, rot, scale,
                Arrays.asList(Color.RED, Color.RED,
                        Color.BLUE, Color.BLUE,
                        Color.ORANGE, Color.ORANGE,
                        Color.AQUA, Color.AQUA,
                        Color.YELLOW, Color.YELLOW,
                        Color.GREEN, Color.GREEN
                        /*Color.LIGHTGREY, Color.LIGHTGREY,
                        Color.LIGHTGREY, Color.LIGHTGREY,
                        Color.LIGHTGREY, Color.LIGHTGREY,
                        Color.LIGHTGREY, Color.LIGHTGREY,
                        Color.LIGHTGREY, Color.LIGHTGREY,
                        Color.LIGHTGREY, Color.LIGHTGREY)*/), Optional.empty());
    }

    @Override
    public void update() {
        triangleMesh2.getRot().add(new Vector3(0.1, 0.3, 0.6));
        if (lerp != null && !lerp.finish()){
            lerp.apply(triangleMesh.getRot());
        }
        if (slerp != null && !slerp.finish()){
            slerp.apply(triangleMesh);
        }
        //triangleMesh2.setPos(test);
        graphicsContext2D.setFill(Color.BLACK);
        graphicsContext2D.fillText("Projection : "+(ApplicationManager.getInstance().getCamera().isSwitchProjection() ? "P" : "O"), 10, 20);
        graphicsContext2D.fillText("Culling Face : "+(ApplicationManager.getInstance().getCamera().isActiveCullingFace() ? "True" : "False"), 100, 20);
        graphicsContext2D.fillText("Colors : "+(ApplicationManager.getInstance().getCamera().isColoredTriangle() ? "True" : "False"), 230, 20);
        graphicsContext2D.fillText("Depth Test : "+(ApplicationManager.getInstance().getCamera().isDepthTest() ? "True" : "False"), 360, 20);
        graphicsContext2D.fillText("Select Triangle : "+(ApplicationManager.getInstance().getCamera().isActiveSelection() ? "True" : "False"), 490, 20);
        graphicsContext2D.fillText("Light Active : "+(ApplicationManager.getInstance().getCamera().isLightActive() ? "True" : "False"), 230, 40);
        Mat4 transformation = triangleMesh.createTransformation();
        switch (toggleRot){
            case 0:
                graphicsContext2D.fillText("Rotation Matrix : "+ String.format(
                        "%.02f %.02f %.02f %.02f\n"+
                                "                            %.02f %.02f %.02f %.02f\n"+
                                "                            %.02f %.02f %.02f %.02f\n"+
                                "                            %.02f %.02f %.02f %.02f\n",
                        transformation.getData()[0][0], transformation.getData()[0][1], transformation.getData()[0][2], transformation.getData()[0][3],
                        transformation.getData()[1][0], transformation.getData()[1][1], transformation.getData()[1][2], transformation.getData()[1][3],
                        transformation.getData()[2][0], transformation.getData()[2][1], transformation.getData()[2][2], transformation.getData()[2][3],
                        transformation.getData()[3][0], transformation.getData()[3][1], transformation.getData()[3][2], transformation.getData()[3][3]
                ), 10, 40);
                break;
            case 1:
                double angle = Math.acos((transformation.getData()[0][0]+transformation.getData()[1][1]+transformation.getData()[2][2]-1) /2);
                double x = (transformation.getData()[2][1]-transformation.getData()[1][2])/ Math.sqrt(
                        Math.pow(transformation.getData()[2][1]-transformation.getData()[1][2], 2)+
                                Math.pow(transformation.getData()[0][2]-transformation.getData()[2][0], 2)+
                                Math.pow(transformation.getData()[1][0]-transformation.getData()[0][1], 2));
                double y = (transformation.getData()[0][2]-transformation.getData()[2][0])/Math.sqrt(
                        Math.pow(transformation.getData()[2][1]-transformation.getData()[1][2], 2)+
                                Math.pow(transformation.getData()[0][2]-transformation.getData()[2][0], 2)+
                                Math.pow(transformation.getData()[1][0]-transformation.getData()[0][1], 2));
                double z = (transformation.getData()[1][0]-transformation.getData()[0][1])/Math.sqrt(
                        Math.pow(transformation.getData()[2][1]-transformation.getData()[1][2], 2)+
                                Math.pow(transformation.getData()[0][2]-transformation.getData()[2][0], 2)+
                                Math.pow(transformation.getData()[1][0]-transformation.getData()[0][1], 2));
                graphicsContext2D.fillText("Axis-Angle : "+ String.format("(%.02f %.02f %.02f), %.02f",
                        x, y, z, angle
                ), 10, 40);
                break;
            case 2:
                Vector3 rot = triangleMesh.getRot();
                double[] d = TriangleMesh.rightAngles(new double[]{rot.getX(), rot.getY(), rot.getZ()});
                graphicsContext2D.fillText("Euler angles : "+ String.format("%.02f° %.02f° %.02f°",
                        d[0], d[1], d[2]
                ), 10, 40);
                System.out.println(Arrays.toString(TriangleMesh.setFromMat(TriangleMesh.fromEuler(rot))));
                break;
            case 3:
                double[] q = TriangleMesh.toQuaternion(triangleMesh.getRot());
                graphicsContext2D.fillText("Quaternion : "+ String.format("%.02f %.02f %.02f %.02f",
                        q[0], q[1], q[2], q[3]
                ), 10, 40);
                break;
        }
    }

    public static void main(String[] args) {
        launch();
    }

}
