package com.example.demo1.demo;

import com.example.demo1.api.TriangleMesh;
import com.example.demo1.api.math.Vector3;
import javafx.scene.paint.Material;

import java.util.Optional;


public class Slerp {
    private double time, timeMax;
    private Vector3 finalCo;
    private double[] q, qFinal;
    private double angle;


    public Slerp(double timeMax, Vector3 actualCo, Vector3 finalCo){
        time = 0;
        this.timeMax = timeMax;
        this.finalCo = finalCo;
        q = TriangleMesh.setFromMat(TriangleMesh.fromEuler(actualCo));
        qFinal = TriangleMesh.setFromMat(TriangleMesh.fromEuler(finalCo));
        angle = q[0]*qFinal[0] + q[1]*qFinal[1] + q[2]*qFinal[2] + q[3]*qFinal[3];

    }

    public void apply(TriangleMesh triangleMesh){
        double t = time / timeMax;
        double maga = Math.sqrt(Math.pow(q[0], 2)+Math.pow(q[1], 2)+Math.pow(q[2], 2)+Math.pow(q[3], 2));
        double magb = Math.sqrt(Math.pow(qFinal[0], 2)+Math.pow(qFinal[1], 2)+Math.pow(qFinal[2], 2)+Math.pow(qFinal[3], 2));
        double omaga = Math.acos(angle / (maga * magb));
        double ratioA = Math.sin((1 - t) * omaga) / Math.sin(omaga);
        double ratioB = Math.sin(t * omaga) / Math.sin(omaga);
        double[] result = new double[]{
                ratioA * q[0] + ratioB * qFinal[0],
                ratioA * q[1] + ratioB * qFinal[1],
                ratioA * q[2] + ratioB * qFinal[2],
                ratioA * q[3] + ratioB * qFinal[3]
        };
        if (++time >= timeMax){
            triangleMesh.setRot(finalCo);
            triangleMesh.setSlerp(Optional.empty());
            return;
        }
        triangleMesh.setSlerp(Optional.of(TriangleMesh.createTransformationMatrix(triangleMesh.getPos(), triangleMesh.getScale(), result)));
    }

    public boolean finish(){
        return time >= timeMax;
    }
}
