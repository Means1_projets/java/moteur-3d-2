package com.example.demo1.demo;

import com.example.demo1.api.ApplicationManager;
import com.example.demo1.api.TriangleMesh;
import com.example.demo1.api.math.Mat4;
import com.example.demo1.api.math.Vector3;
import com.example.demo1.demo.Lerp;
import com.example.demo1.demo.Slerp;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Light;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DemoApp2 extends ApplicationManager {


    private int toggleRot = 0;
    private int n = 20;
    private ArrayList<Point> points = new ArrayList<>();
    private int td;
    private int t = 0;


    @Override
    public void startApp(Stage stage, GraphicsContext graphicsContext2D) {
        stage.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.getCode() == KeyCode.NUMPAD4) {
                toggleRot = (toggleRot + 1) % 5;
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD5) {
                td = (td + 1) % 3;
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD6){
                points.clear();
                for (int i = 0; i < n; i++) {
                    points.add(new Point(new Random().nextInt(500), new Random().nextInt(500)));
                }
            }
            if (keyEvent.getCode() == KeyCode.NUMPAD2){
                t = 0;
            }
        });
    }


    @Override
    public void update() {
        graphicsContext2D.setFill(Color.BLACK);
        if (td == 0) {
            graphicsContext2D.strokePolygon(new double[]{100, 200}, new double[]{graphicsContext2D.getCanvas().getHeight() - 100, graphicsContext2D.getCanvas().getHeight() - 200}, 2);

            Point a = new Point(100, 100);
            Point b = new Point(200, 200);

            Point m = null;
            if (toggleRot == 0) {
                m = new Point(100, 200);
            } else if (toggleRot == 1) {
                m = new Point(150, 150);
            } else if (toggleRot == 2) {
                m = new Point(100, 0);
            } else if (toggleRot == 3) {
                m = new Point(50, 50);
            } else if (toggleRot == 4) {
                m = new Point(250, 250);
            }

            Point am = new Point(m.x - a.x, m.y - a.y);
            Point bm = new Point(m.x - b.x, m.y - b.y);
            Point ab = new Point(b.x - a.x, b.y - a.y);
            Point ba = new Point(a.x - b.x, a.y - b.y);

            double det = am.x * ab.y - am.y * ab.x;

            double aml = am.length();
            double abl = ab.length();
            double bml = bm.length();
            double bal = ba.length();

            double scalaire = (am.x * ab.x + am.y * ab.y) / (aml * abl);
            double scalaire2 = (bm.x * ba.x + bm.y * ba.y) / (bml * bal);


            graphicsContext2D.fillOval(m.x - 5, graphicsContext2D.getCanvas().getHeight() - m.y - 5, 10, 10);


            graphicsContext2D.fillText("droite : " + (det < 0 ? "dessus" : det > 0 ? "dessous" : "sur") + ", segment : " +
                    (scalaire < 0 && scalaire2 > 0 ?
                            "avant" : scalaire > 0 && scalaire2 < 0 ?
                            "après" : "dedans"), 10, 20);


        } else if (td == 1) {
            int nb = 20;
            points.clear();
            for (int i = 0; i < n; i++) {
                points.add(new Point(new Random().nextInt(500), new Random().nextInt(500)));
            }
            for (Point point : points) {
                graphicsContext2D.fillOval(point.x - 5, graphicsContext2D.getCanvas().getHeight() - point.y - 5, 10, 10);
            }
            List<Vector> vectors = new ArrayList<>();

            compute(vectors, points);
            //triangleMesh2.getRot().add(new Vector3(1, 3, 6));
            for (Vector vec : vectors) {
                graphicsContext2D.strokePolygon(new double[]{vec.p1.x, vec.p2.x}, new double[]{graphicsContext2D.getCanvas().getHeight() - vec.p1.y, graphicsContext2D.getCanvas().getHeight() - vec.p2.y}, 2);
            }

            ArrayList<Point> points2 = new ArrayList<>(points);
            Point point = new Point(400, 400);
            points2.add(point);
            List<Vector> vectors2 = new ArrayList<>();
            compute(vectors2, points2);
            if (vectors2.stream().anyMatch(p -> p.p1==point || p.p2==point)){
                graphicsContext2D.setFill(Color.RED);
            } else {
                graphicsContext2D.setFill(Color.GREEN);
            }
            graphicsContext2D.fillOval(point.x - 5, graphicsContext2D.getCanvas().getHeight() - point.y - 5, 10, 10);

        } else if (td == 2){
            t += 5;
            graphicsContext2D.strokePolygon(new double[]{0, 1000}, new double[]{t, t}, 2);
            List<Vector> vectors = Arrays.asList(new Vector(new Point(100, 100), new Point(200, 200)),
                    new Vector(new Point(200, 100), new Point(100, 200)));

            List<Vector> collect = vectors.stream().map(p -> p.getP1().getY() > p.getP2().getY() ? p : new Vector(p.getP2(), p.getP1())).collect(Collectors.toList());

            System.out.println(collect);
            for (Vector vec : collect) {
                graphicsContext2D.strokePolygon(new double[]{vec.p1.x, vec.p2.x}, new double[]{graphicsContext2D.getCanvas().getHeight() - vec.p1.y, graphicsContext2D.getCanvas().getHeight() - vec.p2.y}, 2);
            }
        }
    }

    private void compute(List<Vector> vectors, ArrayList<Point> points) {
        for (int i = 0; i < points.size(); i++) {
            for (int j = i; j < points.size(); j++) {
                Point pointi = points.get(i);
                Point pointj = points.get(j);

                boolean is = true;
                int side = 0;
                for (Point pointk : points) {
                    Point ik = new Point(pointk.x - pointi.x, pointk.y - pointi.y);
                    Point ij = new Point(pointj.x - pointi.x, pointj.y - pointi.y);

                    double det = ik.x * ij.y - ik.y * ij.x;
                    if (side == 0 && det > 0 || side == 1 && det > 0){
                        side = 1;
                    } else if (side == 0 && det < 0 || side == 2 && det < 0){
                        side = 2;
                    } else if (pointi != pointk && pointj != pointk){
                        is = false;
                        break;
                    }
                }
                if (is){
                    vectors.add(new Vector(pointi, pointj));
                }
            }
        }
    }

    public static void main(String[] args) {
        launch();
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @ToString
    static class Point
    {
        private double x, y;

        public double length(){
            return Math.sqrt(x*x + y*y);
        }
    }
    @Getter
    @Setter
    @AllArgsConstructor
    @ToString
    static class Vector
    {
        private Point p1, p2;
    }

}
