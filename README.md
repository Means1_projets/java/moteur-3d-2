# moteur-3d-2

Projet réalisé durant la dernière année d'ingénieur à CYTech, à la différence du [TIPE](https://gitlab.com/Means1_projets/java/moteur-3d), il n'y a pas de binding à opengl, pas de shader, vbo, ...
Utilisation uniquement de Java et JavaFx pour avoir un canvas


![Image](img/prez.png)

Il y a la possibilité d'activer et de désactiver certaines options, cela permet de mieux comprendre les étapes qui permettent d'avoir le rendu sur l'écran

R : Active/Désactive la lumière 

T : Active/Désactive la visualisation du triangle que la caméra regarde

Y : Active/Désactive le test de profondeur (depth test)

U : Active/Désactive l'option qui permet d'avoir des triangles rempli (filled triangle)

I : Active/Désactive le test pour cacher les faces non visible (culling Face)

O : Affiche la rotation de 4 manières différentes : 

- Euler Angle
- Matrix rotation
- Axis-Angle
- Quaternions

P : Change le type de projection : orthographique / perspective

M : active un Slerp sur l'objet pour le remettre à la rotation initiale

L : active un Lerp sur l'objet pour le remettre à la rotation initiale

NUMPAD(2, 4, 6, 8, 7, 9) : Change la rotation de l'objet
